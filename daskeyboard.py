#!/usr/bin/env python3 
import json
import requests
import time
from matplotlib import colors

url = 'http://localhost:27301'
pid = 'DK4QPID'
headers = { "Content-type": "application/json"} # http headers

def daskeyboard(**user):
	""" low level function for das keyboard POST
	"""
	# prepare signal
	signal = {
		'pid': pid,
		'name': ''
	}
	signal.update(user) # override with items that the user provides

	# send signal
	result = requests.post(url + '/api/1.0/signals', data=json.dumps(signal), headers=headers)

	# checking the response
	if result.ok:
		return json.loads(result.text)
	else:
		raise Exception('DasKeyboard', result.text)

def keyColor(key, color):
	""" set key to color, color can be either a name such as 'pink', a 3 digit hexadecimal rgb code,
	such as #f00 for red or a 6 digit hexadecimal rgb code, such as #ff0000 for red.
	"""
	try:
		color = colors.cnames[color]
	except:
		pass
	return daskeyboard(effect='SET_COLOR', zoneId="KEY_"+key.upper(), color=color)

def color(color):
	" set all keys to a certain color "
	try:
		color = colors.cnames[color]
	except:
		pass
	for i in range(0, 6):
		for j in range(0, 24):
			daskeyboard(effect='SET_COLOR', zoneId="{:d},{:d}".format(j, i), color=color)
		time.sleep(0.2)
	
def reset():
	""" get recent signals (shadows) and remove them, setting keyboard to default state 
	"""
	result = requests.get(url + '/api/1.0/signals/pid/'+pid+'/shadows', headers=headers)

	# checking the response
	if result.ok:
		shadows = json.loads(result.text)
	else:
		raise Exception('DasKeyboard', result.text)
	
	for signal in shadows:
		result = requests.delete(url + '/api/1.0/signals/pid/'+ pid + '/zoneId/' + signal['zoneId'],
			headers=headers)
		#if not result.ok:
			#raise Exception('DasKeyboard', result.text)
		time.sleep(0.1)
		
		

if __name__ == "__main__":
	#daskeyboard(zoneId="KEY_Q", color="#000")
	#print(keyColor("Q", "red"))
	reset()
	#color("purple")
	
