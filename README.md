Put daskeyboard.py in the same directory as the experiment or in a directory in
the Python path. In a Python script you can use the Das Keyboard 4Q with for instance 
the following code:

```python
#!/usr/bin/env python3
import time
import daskeyboard

daskeyboard.color("red") # all keys red, using #f00 does the same
time.sleep(3)
daskeyboard.keyColor("G", "blue") # G key becomes blue
time.sleep(3)
daskeyboard.reset() # reset to the color set in the Q software RGB profile

```
